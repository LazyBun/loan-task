package pl.piszkod.loantask

import org.junit.jupiter.api.Test
import pl.piszkod.loantask.domain.Loan
import pl.piszkod.loantask.domain.LoanAmount
import pl.piszkod.loantask.domain.LoanStart
import pl.piszkod.loantask.domain.Term
import java.math.BigDecimal
import java.time.OffsetDateTime
import java.time.ZoneOffset

import org.assertj.core.api.Assertions.assertThat

class LoanTest {

    private val zoneId = ZoneOffset.UTC

    @Test
    fun `Due date calculation`() {
        assertThat(
            Loan.new(
                loanedAmount = LoanAmount(BigDecimal.ZERO),
                loanStartDate = LoanStart(OffsetDateTime.of(2020, 1, 1, 0, 0, 0, 0, zoneId)),
                Term(30),
            ).dueDate.value
        ).isEqualTo(OffsetDateTime.of(2020, 1, 31, 0, 0, 0, 0, zoneId))

        assertThat(
            Loan.new(
                loanedAmount = LoanAmount(BigDecimal.ZERO),
                loanStartDate = LoanStart(OffsetDateTime.of(2020, 1, 1, 0, 0, 0, 0, zoneId)),
                Term(2),
            ).dueDate.value
        ).isEqualTo(OffsetDateTime.of(2020, 1, 3, 0, 0, 0, 0, zoneId))

        assertThat(
            Loan.new(
                loanedAmount = LoanAmount(BigDecimal.ZERO),
                loanStartDate = LoanStart(OffsetDateTime.of(2020, 1, 1, 0, 0, 0, 0, zoneId)),
                Term(650),
            ).dueDate.value
        ).isEqualTo(OffsetDateTime.of(2021, 10, 12, 0, 0, 0, 0, zoneId))
    }

    @Test
    fun `Due amount calculation`() {
        val someDate = LoanStart(OffsetDateTime.now())
        val someTerm = Term(30)

        assertThat(
            Loan.new(
                loanedAmount = LoanAmount(BigDecimal("100.00")),
                loanStartDate = someDate,
                term = someTerm,
            ).dueAmount.value
        ).isEqualTo(BigDecimal("110.00"))

        assertThat(
            Loan.new(
                loanedAmount = LoanAmount(BigDecimal("2137.69")),
                loanStartDate = someDate,
                term = someTerm,
            ).dueAmount.value
        ).isEqualTo(BigDecimal("2351.45"))

        assertThat(
            Loan.new(
                loanedAmount = LoanAmount(BigDecimal("0.0")),
                loanStartDate = someDate,
                term = someTerm,
            ).dueAmount.value
        ).isEqualTo(BigDecimal("0.00"))
    }

    @Test
    fun `Loan extension`() {
        assertThat(
            Loan.new(
                loanedAmount = LoanAmount(BigDecimal.ZERO),
                loanStartDate = LoanStart(OffsetDateTime.of(2020, 1, 1, 0, 0, 0, 0, zoneId)),
                Term(650),
            ).withExtension(10).dueDate.value
        ).isEqualTo(OffsetDateTime.of(2021, 10, 22, 0, 0, 0, 0, zoneId))
    }

}
