package pl.piszkod.loantask

import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Test
import org.mockito.kotlin.*
import pl.piszkod.loantask.application.*
import pl.piszkod.loantask.domain.*
import pl.piszkod.loantask.repository.LoanRepository
import java.math.BigDecimal
import java.time.Instant
import java.time.OffsetDateTime


class LoanServiceTest {

    private val loanRepositoryMock = mock<LoanRepository>()
    private val fakeClock = FakeClock()
    private val loanServiceSettings = LoanServiceSettings(
        100, 1, 10, BigDecimal("1000"), BigDecimal("10")
    )


    val loanAmount = LoanAmount(BigDecimal("100"))
    val term = Term(10)
    val loanToReturn = Loan(
        LoanId("1"),
        loanAmount,
        LoanStart(OffsetDateTime.now(),),
        DueDate(OffsetDateTime.now()),
        DueAmount(BigDecimal("10")),
        InterestRate(BigDecimal("10")),
    )

    @Test
    fun `Should add loan`() {
        fakeClock.fixAt(Instant.parse("2020-01-01T08:00:00Z"))
        val loanService = LoanService(
            loanServiceSettings,
            loanRepositoryMock,
            fakeClock
        )

        whenever(loanRepositoryMock.saveOrUpdate(any())).thenReturn(
            loanToReturn
        )

        loanService.new(loanAmount, term)
    }

    @Test
    fun `Should reject loan on time is between midnight and 6 am`() {
        fakeClock.fixAt(Instant.parse("2020-01-01T03:00:00Z"))
        val loanService = LoanService(
            loanServiceSettings,
            loanRepositoryMock,
            fakeClock
        )
        val loanAmount = LoanAmount(BigDecimal("100"))
        val term = Term(10)

        assertThatThrownBy {
            loanService.new(loanAmount, term)
        }.isInstanceOf(NewLoanRequestedDuringIllegalHours::class.java)

        verifyNoInteractions(loanRepositoryMock)
    }

    @Test
    fun `Should reject loan if term limits are exceeded`() {
        fakeClock.fixAt(Instant.parse("2020-01-01T08:00:00Z"))
        val loanService = LoanService(
            loanServiceSettings.copy(
                minTermDuration = 1,
                maxTermDuration = 8
            ),
            loanRepositoryMock,
            fakeClock
        )
        val loanAmount = LoanAmount(BigDecimal("100"))
        val term = Term(10)

        assertThatThrownBy {
            loanService.new(loanAmount, term)
        }.isInstanceOf(OutsideNewLoanTermLimit::class.java)

        verifyNoInteractions(loanRepositoryMock)
    }

    @Test
    fun `Should reject loan if loan amount limits are exceeded`() {
        fakeClock.fixAt(Instant.parse("2020-01-01T08:00:00Z"))
        val loanService = LoanService(
            loanServiceSettings.copy(
                maxLoanAmount = BigDecimal("100"),
                minLoanAmount = BigDecimal("10"),
            ),
            loanRepositoryMock,
            fakeClock
        )
        val loanAmount = LoanAmount(BigDecimal("1000"))
        val term = Term(10)

        assertThatThrownBy {
            loanService.new(loanAmount, term)
        }.isInstanceOf(OutsideNewLoanAmountLimit::class.java)

        verifyNoInteractions(loanRepositoryMock)
    }

    @Test
    fun `Should throw if no loan is found when getting loan`() {
        fakeClock.fixAt(Instant.parse("2020-01-01T08:00:00Z"))
        val loanId = LoanId("1")
        val loanService = LoanService(
            loanServiceSettings,
            loanRepositoryMock,
            fakeClock
        )

        whenever(loanRepositoryMock.tryGet(loanId)).thenReturn(null)

        assertThatThrownBy {
            loanService.get(loanId)
        }.isInstanceOf(LoanNotFound::class.java)
    }

    @Test
    fun `Should extend loan`() {
        fakeClock.fixAt(Instant.parse("2020-01-01T08:00:00Z"))
        val loanId = LoanId("1")
        val loanService = LoanService(
            loanServiceSettings,
            loanRepositoryMock,
            fakeClock
        )

        whenever(loanRepositoryMock.tryGet(loanId)).thenReturn(loanToReturn)
        whenever(loanRepositoryMock.saveOrUpdate(any())).thenReturn(loanToReturn)

        loanService.extend(loanId)
    }

    @Test
    fun `Should throw if not loan is found when extending loan`() {
        fakeClock.fixAt(Instant.parse("2020-01-01T08:00:00Z"))
        val loanId = LoanId("1")
        val loanService = LoanService(
            loanServiceSettings,
            loanRepositoryMock,
            fakeClock
        )

        whenever(loanRepositoryMock.tryGet(loanId)).thenReturn(null)

        assertThatThrownBy {
            loanService.extend(loanId)
        }.isInstanceOf(LoanNotFound::class.java)
    }

}
