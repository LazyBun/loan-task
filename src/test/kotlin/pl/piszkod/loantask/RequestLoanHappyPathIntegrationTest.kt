package pl.piszkod.loantask

import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpPost
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.TestConstructor
import java.time.Clock
import java.time.Instant
import java.time.ZoneId
import net.pwall.json.test.JSONExpect.Companion.expectJSON
import java.math.BigDecimal


@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestConstructor(autowireMode = TestConstructor.AutowireMode.ALL)
class RequestLoanHappyPathIntegrationTest(
    @Value("\${local.server.port}") private val port: Int,
    private val clock: FakeClock
) {

    @Test
    fun `Creates loan`() {
        clock.fixAt(Instant.parse("2020-01-01T08:00:00Z"))

        val (_, createResponse, createResult) = "http://localhost:$port/loan"
            .httpPost()
            .header("Content-Type", "application/json").body(
            """{
               "term": 100,
               "amount": 10000.0
            }""".trimIndent()
        ).responseString()

        assertThat(createResponse.statusCode).isEqualTo(200)
        println(createResult.get())

        var createdId = ""

        expectJSON(createResult.get()) {
            property("id") {
                createdId = nodeAsString
            }
            property("loanedAmount", BigDecimal("10000.00"))
            property("dueAmount", BigDecimal("11000.00"))
            property("dueDate", "2020-04-10T08:00:00Z")
        }

        val (_, getResponse, getResult) = "http://localhost:$port/loan/$createdId"
            .httpGet()
            .responseString()

        assertThat(getResponse.statusCode).isEqualTo(200)
        expectJSON(createResult.get()) {
            property("dueAmount", BigDecimal("11000.00"))
            property("dueDate", "2020-04-10T08:00:00Z")
        }

    }

}


@Configuration
class FakeBeans {

    @Bean
    @Primary
    fun fakeClock(): FakeClock = FakeClock()
}

class FakeClock : Clock() {

    private var inner: Clock = systemUTC()

    fun fixAt(now: Instant) {
        inner = fixed(now, ZoneId.of("UTC"))
    }

    override fun instant(): Instant = inner.instant()

    override fun withZone(zone: ZoneId?): Clock = inner.withZone(zone)

    override fun getZone(): ZoneId = inner.zone

}
