package pl.piszkod.loantask.infra

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import pl.piszkod.loantask.application.LoanServiceSettings
import java.math.BigDecimal
import java.time.Clock

@Configuration
class AppConfiguration {

    @Bean
    fun clock(): Clock = Clock.systemUTC()

    @Bean
    fun loanServiceSettings(
        @Value("\${loans.limits.term-duration-in-days.max}") maxTermDuration: Long,
        @Value("\${loans.limits.term-duration-in-days.min}") minTermDuration: Long,
        @Value("\${loans.extension.term-duration-in-days}") loanExtensionDuration: Long,
        @Value("\${loans.limits.amount.max}") maxLoanAmount: BigDecimal,
        @Value("\${loans.limits.amount.min}") minLoanAmount: BigDecimal,
    ): LoanServiceSettings = LoanServiceSettings(
        maxTermDuration,
        minTermDuration,
        loanExtensionDuration,
        maxLoanAmount,
        minLoanAmount
    )

}
