package pl.piszkod.loantask.infra

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
class ErrorHandler {

    @ExceptionHandler(Exception::class)
    fun handle(ex: Exception): ResponseEntity<ErrorDto> {
        return ResponseEntity.internalServerError().body(ErrorDto("General error"))
    }

    @ExceptionHandler(AppError::class)
    fun handle(ex: AppError): ResponseEntity<ErrorDto> {
        return ResponseEntity
            .status(ex.code)
            .body(ErrorDto(ex.message))
    }

}


open class AppError(override val message: String, val code: Int) : Exception(message)

data class ErrorDto(val message: String)
