package pl.piszkod.loantask.repository

import org.springframework.stereotype.Service
import pl.piszkod.loantask.domain.Loan
import pl.piszkod.loantask.domain.LoanId

interface LoanRepository {

    fun saveOrUpdate(loan: Loan): Loan

    fun tryGet(id: LoanId): Loan?

}


@Service
class InMemoryLoanRepository: LoanRepository {

    private val loans = mutableMapOf<LoanId, Loan>()

    override fun saveOrUpdate(loan: Loan): Loan {
        loans[loan.id] = loan
        return loan
    }

    override fun tryGet(id: LoanId): Loan? {
        return loans[id]
    }

}
