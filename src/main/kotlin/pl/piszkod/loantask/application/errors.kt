package pl.piszkod.loantask.application

import pl.piszkod.loantask.domain.LoanAmount
import pl.piszkod.loantask.domain.LoanId
import pl.piszkod.loantask.domain.Term
import pl.piszkod.loantask.infra.AppError
import java.math.BigDecimal
import java.time.OffsetDateTime


sealed class LoanError(message: String, code: Int) : AppError(message, code)

data class LoanNotFound(val id: LoanId) : LoanError("Loan: [$id] not found", 404)

data class NewLoanRequestedDuringIllegalHours(val now: OffsetDateTime)
    : LoanError("New loan requested during illegal hours: [$now]", 400)
data class OutsideNewLoanTermLimit(val requestedTerm: Term, val minTerm: Long, val maxTerm: Long)
    : LoanError("Tried to create new loan, but requested term: [$requestedTerm] is outside limits - min: [$minTerm], max: [$maxTerm]", 400)

data class OutsideNewLoanAmountLimit(val requestedAmount: LoanAmount, val minAmount: BigDecimal, val maxAmount: BigDecimal)
    : LoanError("Tried to create new loan, but requested amount: [$requestedAmount] is outside limits - min: [$minAmount], max: [$maxAmount]", 400)
