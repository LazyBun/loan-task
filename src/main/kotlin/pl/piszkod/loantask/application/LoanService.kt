package pl.piszkod.loantask.application

import org.springframework.stereotype.Service
import pl.piszkod.loantask.domain.*
import pl.piszkod.loantask.repository.LoanRepository
import java.math.BigDecimal
import java.time.Clock
import java.time.OffsetDateTime

@Service
class LoanService(
    private val settings: LoanServiceSettings,
    private val loanRepository: LoanRepository,
    private val clock: Clock
) {

    companion object {
        private const val LOAN_REQUEST_RESTRICTION_HOUR_START = 0
        private const val LOAN_REQUEST_RESTRICTION_HOUR_END = 6
    }

    fun new(amount: LoanAmount, term: Term): Loan {
        val now = OffsetDateTime.now(clock)

        if (isRequestedDuringIllegalHours(now)) {
            throw NewLoanRequestedDuringIllegalHours(now)
        }
        if (isNotWithinDurationLimits(term)) {
            throw OutsideNewLoanTermLimit(term, settings.minTermDuration, settings.maxTermDuration)
        }
        if (isNotWithinAmountLimits(amount)) {
            throw OutsideNewLoanAmountLimit(amount, settings.minLoanAmount, settings.maxLoanAmount)
        }

        return loanRepository.saveOrUpdate(Loan.new(amount, LoanStart(now), term))
    }


    private fun isRequestedDuringIllegalHours(now: OffsetDateTime) =
        now.hour >= LOAN_REQUEST_RESTRICTION_HOUR_START && now.hour < LOAN_REQUEST_RESTRICTION_HOUR_END
    private fun isNotWithinDurationLimits(term: Term) =
        (term.days in settings.minTermDuration..settings.maxTermDuration).not()

    private fun isNotWithinAmountLimits(amount: LoanAmount) =
        (amount.value in settings.minLoanAmount..settings.maxLoanAmount).not()

    fun get(id: LoanId): Loan {
        return loanRepository.tryGet(id)
            ?: throw LoanNotFound(id)
    }

    fun extend(id: LoanId): Loan {
        val loan = loanRepository.tryGet(id)
            ?: throw LoanNotFound(id)

        val extendedLoan = loan.withExtension(settings.loanExtensionDuration)

        return loanRepository.saveOrUpdate(extendedLoan)
    }

}

data class LoanServiceSettings(
    val maxTermDuration: Long,
    val minTermDuration: Long,
    val loanExtensionDuration: Long,
    val maxLoanAmount: BigDecimal,
    val minLoanAmount: BigDecimal,
)
