package pl.piszkod.loantask.domain

import java.math.BigDecimal
import java.math.RoundingMode
import java.time.OffsetDateTime
import java.util.*

// Assumption: We don't care about increments that are less than 1 day.
// Assumption: For the sake of this task we consider all dates to be in UTC
data class Loan(
    val id: LoanId,
    val loanedAmount: LoanAmount,
    val loanStartDate: LoanStart,
    val dueDate: DueDate,
    val dueAmount: DueAmount,
    val interestRate: InterestRate
) {
    companion object {
        fun new(
            loanedAmount: LoanAmount,
            loanStartDate: LoanStart,
            term: Term,
            interestRate: InterestRate = InterestRate(BigDecimal("0.1"))
        ) = Loan(
            LoanId(UUID.randomUUID().toString()),
            // FIXME ugly
            LoanAmount(loanedAmount.value.setScale(2, RoundingMode.DOWN)),
            loanStartDate,
            DueDate(loanStartDate.value.plusDays(term.days)),
            DueAmount((loanedAmount.value + (loanedAmount.value * interestRate.value)).setScale(2, RoundingMode.DOWN)),
            interestRate
        )

    }

    fun withExtension(extensionInDays: Long): Loan {
        return copy(
            dueDate = DueDate(dueDate.value.plusDays(extensionInDays)),
        )
    }

}

@JvmInline
value class LoanId(val value: String)

@JvmInline
value class LoanAmount(val value: BigDecimal)

@JvmInline
value class InterestRate(val value: BigDecimal)

@JvmInline
value class DueAmount(val value: BigDecimal)

@JvmInline
value class LoanStart(val value: OffsetDateTime)

@JvmInline
value class DueDate(val value: OffsetDateTime)

@JvmInline
value class Term(val days: Long)
