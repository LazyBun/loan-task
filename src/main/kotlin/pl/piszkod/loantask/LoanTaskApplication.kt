package pl.piszkod.loantask

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class LoanTaskApplication

fun main(args: Array<String>) {
	runApplication<LoanTaskApplication>(*args)
}
