package pl.piszkod.loantask.api

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import pl.piszkod.loantask.application.LoanService
import pl.piszkod.loantask.domain.LoanAmount
import pl.piszkod.loantask.domain.LoanId
import pl.piszkod.loantask.domain.Term

@RestController
class LoanController(
    private val loanService: LoanService
) {


    @GetMapping("/loan/{id}")
    fun getLoan(@PathVariable("id") id: String): ResponseEntity<GetLoanResponse> {
        return ResponseEntity.ok().body(
            loanService.get(LoanId(id))
                .toGetLoanResponse()
        )
    }

    @PutMapping("/loan/{id}")
    fun extendLoan(@PathVariable("id") id: String): ResponseEntity<ExtendLoanResponse> {
        return ResponseEntity.ok().body(
            loanService.extend(LoanId(id))
                .toExtendLoanResponse()
        )
    }

    @PostMapping("/loan")
    fun applyForLoan(@RequestBody request: NewLoanRequest): ResponseEntity<NewLoanResponse> {
        return ResponseEntity.ok().body(
            loanService.new(LoanAmount(request.amount), Term(request.term))
                .toNewLoanResponse()
        )
    }


}
