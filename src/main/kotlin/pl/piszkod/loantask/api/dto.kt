package pl.piszkod.loantask.api

import pl.piszkod.loantask.domain.Loan
import java.math.BigDecimal
import java.time.OffsetDateTime

data class GetLoanResponse(
    val dueAmount: BigDecimal,
    val dueDate: OffsetDateTime
)

fun Loan.toGetLoanResponse() = GetLoanResponse(
    dueAmount = dueAmount.value,
    dueDate = dueDate.value
)

data class ExtendLoanResponse(
    val dueDate: OffsetDateTime
)

fun Loan.toExtendLoanResponse() = ExtendLoanResponse(
    dueDate = dueDate.value
)

data class NewLoanRequest(
    val amount: BigDecimal,
    val term: Long
)

data class NewLoanResponse(
    val id: String,
    val loanedAmount: BigDecimal,
    val dueAmount: BigDecimal,
    val dueDate: OffsetDateTime
)

fun Loan.toNewLoanResponse() = NewLoanResponse(
    id = id.value,
    loanedAmount = loanedAmount.value,
    dueAmount = dueAmount.value,
    dueDate = dueDate.value,

)
