# Loan Task

Tech interview task implementation. Timeout: 3.5 hours.

## Task description

App for loan applications

Tech requirements:

Spring Boot
REST interface

available operations are:
* apply for loan (term, amount)

if application is not within amount/term range then reject application
if application is between 00:00 and 06:00 and max amount is asked then reject application
issued loan has 10% of principal (not 10% per year)

* extend loan

extension term is preconfigured. Upon extension the due date is changed, original due date + term

* fetch loan

should return amount, due date


define max/min amount and max/min term (days)
no installments

junit tests
integration success path scenario test (loan issued)
no GUI
no authorization
no users

Non-tech requirements:

* code has to be easy to extend/modify
* SOLID, KISS, DRY, etc
* post the solution to a public repo (github, bitbucket, etc)



## Q&A

* Is storing data in memory good enough?

Yes, in memory storage is sufficient.

* Just to clarify, does "issued loan has 10% of principal (not 10% per year)" mean that taking $100 credit should lead to $110 payback?

Yes.

## Known issues after coding timeout

* Unit tests for Loan Service Test could have been better
* Complete lack of logging
* Timezone handling complexity was disregarded (using UTC everywhere)
